# Dpp

Dpp is a python library to sample determinantal point processes, restricted
on a compact disk. Two kernels are available :

- the kernel `ginibre` that generates a Ginibre ensemble : 
```math
K(z, w) = \frac 1 \pi e^{-\frac 1 2 (|z|^2 + |w|^2) - z \overline w}
```
- the kernel `gaussian` that generates the zero set of a Gaussian analytic function: 
```math
K(z, w) = \frac 1 {\pi (1 - z \overline w)^2}
```

The library can be used on the command line. The following will spawn a window
showing the generation of a Ginibre ensemble restricted to a disk of radius
30.
```bash
python3 dpp.py --kernel ginibre 30
```

![![Ginibre ensemble](ginibre_radius30.jpg)](ginibre_radius30.png)

Or without gui, the points can be recorded directly in a file

```bash
python3 dpp.py --nogui --kernel gaussian 0.999 --output points.txt
```

Note that the radius for the kernel `gaussian` must be between 0 and 1.

It can also be used as a python module. In the following, the list L will
contain the list of points as a numpy array of complex numbers.
```python
import dpp
L = dpp.sample(30, kernel=dpp.kernels['ginibre'])
```

# Installation

Dpp depends on the python libraries `numpy`, `numba`, `scipy`, and
optionally the python libraries `pyqtgraph`,`PyQt5` and the c++ library
`Qt5` for the graphic interface.

You can either download the script file dpp.py directly
[here](https://gitlab.inria.fr/gmoro/point_process/raw/master/test.py?inline=false)
or clone the git repository with the following command.
```bash
git clone git@gitlab.inria.fr:gmoro/point_process.git
```

# Command line parameters
The behaviour of the simulations can be controlled by several parameters, detailed below.

```
usage: dpp.py [-h] [--N N] [--kernel kernel] [--precision prec] [--size size] [--refresh refresh] [--error] [--profile] [--quiet] [--nogui] [--output output] R

positional arguments:
  R                  radius

optional arguments:
  -h, --help         show this help message and exit
  --N N              preset N points by truncating the kernel to the N first eigenfunctions (default: None)
  --kernel kernel    kernel to sample : ginibre or gaussian (default: ginibre)
  --precision prec   error tolerated for internal computations (default: 1.1102230246251565e-16)
  --size size        points size in pixels (default: 5)
  --refresh refresh  refresh time in miliseconds (default: 100)
  --error            compute the error and the condition number for the result (default: False)
  --profile          output time indicator some functions (default: False)
  --quiet            disable information messages on standard output (default: False)
  --nogui            output points coordinate on the terminal (default: False)
  --output output    name of file to output the data, implies --nogui (default: None)
```

