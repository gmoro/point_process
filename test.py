import dpp
import sys
import tempfile
import itertools
import numpy as np

np.random.seed(0)

f = tempfile.TemporaryFile(mode='w+t')

arg_options = [(10,), (None, 100, 500), (dpp.kernels['ginibre'],), (2**-53,1e-3), (True, False), (True, False), (None, sys.stdout, f)]
for args in itertools.product(*arg_options):
    print("\n## Testing Ginibre with args {0}".format(args))
    dpp.sample(*args)

arg_options = [(0.99,), (None, 50, 200), (dpp.kernels['gaussian'],), (2**-53,1e-3), (True, False), (True, False), (None, sys.stdout, f)]
for args in itertools.product(*arg_options):
    print("\n## Testing Gaussian with args {0}".format(args))
    dpp.sample(*args)

f.close()
